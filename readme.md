GenericLogger

Create a logger instance:
```php
$kafkaLogger = GenericLogger::getLogger(new Kafka(), 'warning');
```

Log:
```php
$kafkaLogger->error('Missing subscriber info');
```


Improvement ideas:
1. Implement line formatter class/interface instead of function formatLine(), then add timestamp and any additional useful info to log ines.
2. Based on development framework implement a service to maintain the list of logger instances using different targets.
3. Based on development framework refactor the method getLogger() and use dependency injection.
4. Consider refactoring the function storeLogEntry() and array $LOG_LEVELS to improve performance (current solution uses strings as index array).
5. Implement unit tests.
6. Use return value of TargetInterface::sendLine() to detect errors/exceptions.


