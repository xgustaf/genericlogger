<?php
namespace GenericLogger;

/**
 * GenericLogger
 */
class GenericLogger
{
    private static array $LOG_LEVELS = [
        'debug' => 90,
        'info' => 80,
        'warning' => 70,
        'error' => 60
    ];

    private TargetInterface $target;

    /**
     * Private constructor, private, use factory method instead
     * @param TargetInterface $target
     * @param string          $level
     */
    private function __construct(TargetInterface $target, string $level = 'debug')
    {
        $this->target = $target;
        $this->target->init();
        $this->logLevel = $level;
    }

    /**
     * Factory method, creates a logger instance with given target and log level
     * @param TargetInterface $target
     * @param string          $level
     *
     * @return GenericLogger
     */
    public static function getLogger(TargetInterface $target, string $level = 'debug'): GenericLogger
    {
        return new GenericLogger($target, $level);
    }

    private string $logLevel = 'error';

    /**
     * @param string $message
     * @param string $level
     *
     */
    private function storeLogEntry(string $message, string $level = 'debug')
    {
        if (self::$LOG_LEVELS[$level] <= self::$LOG_LEVELS[$this->logLevel]) {
            $this->target->sendLine($this->formatLine($message, $level));
        }
    }

    private function formatLine(string $message, string $level): string
    {
        return '[' . $level . '] ' . $message . PHP_EOL;
    }

    /**
     * @param $level
     */
    public function setLevel($level)
    {
        $this->logLevel = $level;
    }

    /**
     * @param $message
     *
     */
    public function debug($message)
    {
        self::storeLogEntry($message, 'debug');
    }

    /**
     * @param $message
     *
     */
    public function info($message)
    {
        self::storeLogEntry($message, 'info');
    }

    /**
     * @param $message
     *
     */
    public function warning($message)
    {
        self::storeLogEntry($message, 'warning');
    }

    /**
     * @param $message
     *
     */
    public function error($message)
    {
        self::storeLogEntry($message, 'error');
    }
}
