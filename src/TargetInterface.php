<?php
namespace GenericLogger;

/**
 *
 */
interface TargetInterface
{
    /**
     * Make some preparations, open channels, connect servers etc.
     * @return mixed
     */
    public function init();

    /**
     * Send the string to log
     * @param string $line
     *
     * @return mixed
     */
    public function sendLine(string $line);
}
